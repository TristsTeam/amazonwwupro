package com.gna.wwupro;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.gna.wwupro.Infrastructure.KeepScreenOnBaseClass;

public class WWUActivity extends KeepScreenOnBaseClass implements
		OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.wwu);
		keepScreenOn();
		TextView textView = (TextView) findViewById(R.id.textView);
		ImageView titleView = (ImageView) findViewById(R.id.tileView);
		if (getIntent().getExtras().getInt("value") == 1) {
			String text = readEula(this, "About.txt").toString();
			textView.setText(text);
			titleView.setBackgroundResource(R.drawable.title_aboutwwu);
		} else if (getIntent().getExtras().getInt("value") == 2) {
			String text = readEula(this, "WhatWWU.txt").toString();
			textView.setText(text);
			titleView.setBackgroundResource(R.drawable.title_whatiswwu);
		} else if (getIntent().getExtras().getInt("value") == 3) {
			String text = readEula(this, "Instruction.txt").toString();
			textView.setText(text);
			titleView.setBackgroundResource(R.drawable.title_instructions);
		}
		ImageButton leftArrowBtn = (ImageButton) findViewById(R.id.leftArrowClick);
		leftArrowBtn.setOnClickListener(this);
		TextView footerText = (TextView) findViewById(R.id.fotterText);
		if (getResources().getBoolean(R.bool.isTablet)) {
			textView.setTextSize(25);
			footerText.setTextSize(25);
		}

	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		// MediaPlayer mp = MediaPlayer.create(this, R.raw.button);
		// mp.start();
		if (view.getId() == R.id.leftArrowClick) {
			this.finish();
		}
	}

	private static CharSequence readEula(Activity activity, String fileName) {
		InputStream in = null;
		// String fileName = "About.txt";
		try {
			in = activity.getAssets().open(fileName);
			int size = in.available();
			byte[] buffer = new byte[size];
			in.read(buffer);
			String text = new String(buffer);
			buffer = null;
			return text;
		} catch (IOException e) {
			return "";
		} finally {
			closeStream(in);
		}
	}

	private static void closeStream(Closeable stream) {
		if (stream != null) {
			try {
				stream.close();
			} catch (IOException e) {
				// Ignore
			}
		}
	}

}

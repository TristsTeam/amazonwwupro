package com.gna.wwupro.Infrastructure;

import com.gna.wwupro.R;

import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;



public class BaseDisplayExerciseClass extends CommonBaseClass {
	protected String[] exerciseTextArray;
	protected String[] exerciseNameArray;
	protected int[] firstPictureArray;
	protected int[] secondPictureArray;

	protected ImageView firstImage;
	protected ImageView secondImage;
	protected ImageButton rightArrowBtn;
	protected ImageButton leftArrowBtn;
	protected ImageButton cancelBtn;
	protected TextView exerciseText;
	protected TextView exerciseCount;
	protected TextView exerciseName;
	protected ImageView titleView;
	protected ImageButton infoBtn;

	public void intializeViews() {
		firstImage = (ImageView) findViewById(R.id.firstImage);
		secondImage = (ImageView) findViewById(R.id.secondImage);
		rightArrowBtn = (ImageButton) findViewById(R.id.rightArrowBtn);
		leftArrowBtn = (ImageButton) findViewById(R.id.leftArrowBtn);
		exerciseText = (TextView) findViewById(R.id.exerciseText);
		exerciseCount = (TextView) findViewById(R.id.exerciseCount);
		exerciseName = (TextView) findViewById(R.id.exerciseName);
		titleView = (ImageView) findViewById(R.id.titleView);
		cancelBtn = (ImageButton) findViewById(R.id.cancelBtn);
		infoBtn = (ImageButton) findViewById(R.id.infoBtn);

	}

	public void ultimateExercise() {
		titleView.setBackgroundResource(R.drawable.uwdisplay);
		exerciseTextArray = new String[5];
		exerciseNameArray = new String[5];
		firstPictureArray = new int[5];
		secondPictureArray = new int[5];

		exerciseTextArray[0] = "With hands shoulder width apart, core tensed, come down slowly and then up. Repeat.";
		exerciseTextArray[1] = "Holding an imaginary ball, step to the left, giving it to your 'teammate', then step back and jump taking an imaginary shot. Repeat the other side.";
		exerciseTextArray[2] = "On your back, arms raised over head, crunch upward, lifting your legs and arms together but splitting your legs apart as you reach the top point. Repeat.";
		exerciseTextArray[3] = "Lunge diagonally to the left, raising your arms straight upward to your head. Repeat the other side.";
		exerciseTextArray[4] = "Resting on your forearms and toes, tense your core. Don't dip arch your hips. (On your knees if beginner)- now climp up on your left hand, your right, then repeat downward to start";

		exerciseNameArray[0] = "Push-up";
		exerciseNameArray[1] = "3 Pointers";
		exerciseNameArray[2] = "Split V-Ups";
		exerciseNameArray[3] = "Diagonal Arm Rise Lunge";
		exerciseNameArray[4] = "Plank Climber";

		firstPictureArray[0] = R.drawable.uwexercise01_01;
		firstPictureArray[1] = R.drawable.uwexercise02_01;
		firstPictureArray[2] = R.drawable.uwexercise03_01;
		firstPictureArray[3] = R.drawable.uwexercise04_01;
		firstPictureArray[4] = R.drawable.uwexercise05_01;

		secondPictureArray[0] = R.drawable.uwexercise01_02;
		secondPictureArray[1] = R.drawable.uwexercise02_02;
		secondPictureArray[2] = R.drawable.uwexercise03_02;
		secondPictureArray[3] = R.drawable.uwexercise04_02;
		secondPictureArray[4] = R.drawable.uwexercise05_02;

	}

	public void militaryMadnessExercise() {
		titleView.setBackgroundResource(R.drawable.mmdisplay);
		exerciseTextArray = new String[6];
		exerciseNameArray = new String[6];
		firstPictureArray = new int[6];
		secondPictureArray = new int[6];

		exerciseTextArray[0] = "Begin standing. Then drop to your hands, and explode your legs backwards to a push up position, then explode your feet back into your chest and then jump upward vertically. Repeat";
		exerciseTextArray[1] = "On your back, tense your core and rise up crunching over touching your left foot with your right hand. Change sides. Repeat";
		exerciseTextArray[2] = "With your feet elevated and your body completely straight, perform a standard push up.";
		exerciseTextArray[3] = "Begin standing. Then drop to your hands and explode your legs backwards to a push up position, then explode your feet back into your chest and then jump upward vertically. Repeat";
		exerciseTextArray[4] = "With a dumbbell in each hand, and up at shoulder level, lunge forward and press the Dbs upward. Return and lower Dbs. Then repeat for the other leg.";
		exerciseTextArray[5] = "In a push up position, raise your knee out to the side and pause as you perform the upward part (or the downward portion) of your push up.";

		exerciseNameArray[0] = "Burpees";
		exerciseNameArray[1] = "Alt. Foot 2 Toe Crunch";
		exerciseNameArray[2] = "Elevated Push Up";
		exerciseNameArray[3] = "Burpees";
		exerciseNameArray[4] = "Alt. DB Lunge Press";
		exerciseNameArray[5] = "Knee Out Push Ups";

		firstPictureArray[0] = R.drawable.mexercise01_01;
		firstPictureArray[1] = R.drawable.mexercise02_01;
		firstPictureArray[2] = R.drawable.mexercise03_01;
		firstPictureArray[3] = R.drawable.mexercise04_01;
		firstPictureArray[4] = R.drawable.mexercise05_01;
		firstPictureArray[5] = R.drawable.mexercise06_01;

		secondPictureArray[0] = R.drawable.mexercise01_02;
		secondPictureArray[1] = R.drawable.mexercise02_02;
		secondPictureArray[2] = R.drawable.mexercise03_02;
		secondPictureArray[3] = R.drawable.mexercise04_02;
		secondPictureArray[4] = R.drawable.mexercise05_02;
		secondPictureArray[5] = R.drawable.mexercise06_02;

	}

	public void elevationExercise() {
		titleView.setBackgroundResource(R.drawable.ewdisplay);
		exerciseTextArray = new String[6];
		exerciseNameArray = new String[6];
		firstPictureArray = new int[6];
		secondPictureArray = new int[6];

		exerciseTextArray[0] = "Using a solid step, perform quick steps onto it and down.";
		exerciseTextArray[1] = "On the end of an elevated box or step, place your left hand on and your right on the floor, perform an offset push up. Repeat other side alternating.";
		exerciseTextArray[2] = "On your back arms at your side or above your head, lift your legs vertical and together, then raise your legs and your hips off the floor. Pause and then repeat.";
		exerciseTextArray[3] = "With your feet elevated and your body completely straight, perform a standard push up.";
		exerciseTextArray[4] = "With feet together, hop upward onto an elevated step or bench and hop down. Repeat";
		exerciseTextArray[5] = "Standing on an elevated surface, bend and walk out on your palms four or five strides. Perform 1 Push Up and walk back. Repeat";

		exerciseNameArray[0] = "Rapid Steps Ups";
		exerciseNameArray[1] = "Single Arm Box Push Up";
		exerciseNameArray[2] = "Sky Pointers";
		exerciseNameArray[3] = "Elevated Push Up";
		exerciseNameArray[4] = "Hop Ups";
		exerciseNameArray[5] = "Elevated Spider Walk Outs";

		firstPictureArray[0] = R.drawable.ewexercise01_01;
		firstPictureArray[1] = R.drawable.ewexercise02_01;
		firstPictureArray[2] = R.drawable.ewexercise03_01;
		firstPictureArray[3] = R.drawable.ewexercise04_01;
		firstPictureArray[4] = R.drawable.ewexercise05_01;
		firstPictureArray[5] = R.drawable.ewexercise06_01;

		secondPictureArray[0] = R.drawable.ewexercise01_02;
		secondPictureArray[1] = R.drawable.ewexercise02_02;
		secondPictureArray[2] = R.drawable.ewexercise03_02;
		secondPictureArray[3] = R.drawable.ewexercise04_02;
		secondPictureArray[4] = R.drawable.ewexercise05_02;
		secondPictureArray[5] = R.drawable.ewexercise06_02;

	}

	public void grooveAExercise() {
		titleView.setBackgroundResource(R.drawable.gadisplay);
		exerciseTextArray = new String[9];
		exerciseNameArray = new String[9];
		firstPictureArray = new int[9];
		secondPictureArray = new int[9];

		exerciseTextArray[0] = "With two cones, etc...Anything from 20-40 feet apart begin to run/sprint from cone to cone, getting down low and touching floor with your hand at each cone. (Perform 10 runs cone-to-cone).";
		exerciseTextArray[1] = "Standard hand position, slow controlled push up.";
		exerciseTextArray[2] = "With two cones, etc...Anything from 20-40 feet apart, begin to run/sprint with a sideways stride from cone to cone getting down low and touching floor with your hand at each cone (perform 10 runs cone-to-cone)";
		exerciseTextArray[3] = "With fingers flared outward from body, perform a push up.";
		exerciseTextArray[4] = "With two cones etc...Anything from 20-40 feet apart begin to run/sprint with a sidewars stride from cone to cone, getting down low and touching floor with your hand at each cone (perform 10 runs cone-to-cone).";
		exerciseTextArray[5] = "With your hand behind your head and elbows outward perform alternating lunges forward. Keep knee bend at 90 degrees in forward position.";
		exerciseTextArray[6] = "With two cones, etc...Anything from 20-40 feet apart, begin to run/sprint with a sideways stride from cone to cone getting down low and touching floor with your hand at each cone (perform 10 runs cone-to-cone)";
		exerciseTextArray[7] = "With one hand out in front and the other one under your shoulder perform a push up. Repeat.";
		exerciseTextArray[8] = "Laying on your back, crunch upward touching your left foot with your left hand. Return and repeat on the other side.";

		exerciseNameArray[0] = "Point 2 Point Fwd Run(10)";
		exerciseNameArray[1] = "Push Ups(15)";
		exerciseNameArray[2] = "Point 2 Point Side Run";
		exerciseNameArray[3] = "Seal Push Ups";
		exerciseNameArray[4] = "Point 2 Point Fwd Run(10)";
		exerciseNameArray[5] = "Prisoner Lunges";
		exerciseNameArray[6] = "Point 2 Point Side Run";
		exerciseNameArray[7] = "Zig Zag Push Up";
		exerciseNameArray[8] = "Half & Half";

		firstPictureArray[0] = R.drawable.gaexercise01_01;
		firstPictureArray[1] = R.drawable.gaexercise02_01;
		firstPictureArray[2] = R.drawable.gaexercise03_01;
		firstPictureArray[3] = R.drawable.gaexercise04_01;
		firstPictureArray[4] = R.drawable.gaexercise05_01;
		firstPictureArray[5] = R.drawable.gaexercise06_01;
		firstPictureArray[6] = R.drawable.gaexercise07_01;
		firstPictureArray[7] = R.drawable.gaexercise08_01;
		firstPictureArray[8] = R.drawable.gaexercise09_01;

		secondPictureArray[0] = R.drawable.gaexercise01_02;
		secondPictureArray[1] = R.drawable.gaexercise02_02;
		secondPictureArray[2] = R.drawable.gaexercise03_02;
		secondPictureArray[3] = R.drawable.gaexercise04_02;
		secondPictureArray[4] = R.drawable.gaexercise05_02;
		secondPictureArray[5] = R.drawable.gaexercise06_02;
		secondPictureArray[6] = R.drawable.gaexercise07_02;
		secondPictureArray[7] = R.drawable.gaexercise08_02;
		secondPictureArray[8] = R.drawable.gaexercise09_02;

	}

	public void grooveBExercise() {
		titleView.setBackgroundResource(R.drawable.gbdisplay);
		exerciseTextArray = new String[7];
		exerciseNameArray = new String[7];
		firstPictureArray = new int[7];
		secondPictureArray = new int[7];

		exerciseTextArray[0] = "With two cones, etc...Anything from 20-40 feet apart, begin to run/sprint with a sideways stride from cone to cone getting down low and touching floor with your hand at each cone (perform 10 runs cone-to-cone)";
		exerciseTextArray[1] = "With two cones, etc...Anything from 20-40 feet apart, begin to hop on one leg from cone to cone changing legs at each cone (perform 10 hops cone-to-cone).";
		exerciseTextArray[2] = "With hand placed under your shoulders and arms pinned tight to your sides, perform a push up.";
		exerciseTextArray[3] = "With two cones, etc...Anything from 20-40 feet apart, begin to hop on both legs from cone to cone (perform 10 hops cone-to-cone).";
		exerciseTextArray[4] = "Holding a dumbbell in each hand, begin in the push position and row weight up to your ribcage, pause then repeat on the other side";
		exerciseTextArray[5] = "Perform a body weight squat but add a small jump as you explode";
		exerciseTextArray[6] = "With two cones, etch...Anything from 20-40 feet apart, begin to run/sprint with a sideways stride from cone to cone, getting down low and touching floor with your hand at each cone (perform 10 runs cone-to-cone).";

		exerciseNameArray[0] = "Point 2 Point Fwd Run(10)";
		exerciseNameArray[1] = "Point 2 Point Single Leg Hop(10)";
		exerciseNameArray[2] = "Close Grip Push Up";
		exerciseNameArray[3] = "Point 2 Point Explosive Hops";
		exerciseNameArray[4] = "Renegade Row";
		exerciseNameArray[5] = "BW Squat Jumps";
		exerciseNameArray[6] = "Point 2 Point Side Runner";

		firstPictureArray[0] = R.drawable.gbexercise01_01;
		firstPictureArray[1] = R.drawable.gbexercise02_01;
		firstPictureArray[2] = R.drawable.gbexercise03_01;
		firstPictureArray[3] = R.drawable.gbexercise04_01;
		firstPictureArray[4] = R.drawable.gbexercise05_01;
		firstPictureArray[5] = R.drawable.gbexercise06_01;
		firstPictureArray[6] = R.drawable.gbexercise07_01;

		secondPictureArray[0] = R.drawable.gbexercise01_02;
		secondPictureArray[1] = R.drawable.gbexercise02_02;
		secondPictureArray[2] = R.drawable.gbexercise03_02;
		secondPictureArray[3] = R.drawable.gbexercise04_02;
		secondPictureArray[4] = R.drawable.gbexercise05_02;
		secondPictureArray[5] = R.drawable.gbexercise06_02;
		secondPictureArray[6] = R.drawable.gbexercise07_02;

	}

	public void resistanceExercise() {
		titleView.setBackgroundResource(R.drawable.trdisplay);
		exerciseTextArray = new String[5];
		exerciseNameArray = new String[5];
		firstPictureArray = new int[5];
		secondPictureArray = new int[5];

		exerciseTextArray[0] = "Holding a dumbbell in each hand, up at shoulder level, squat downward as you explode up thrust the dumbbells into the air (shoulder press) and then return and repeat.";
		exerciseTextArray[1] = "Holding a light dumbbell in each hand, get in a bent knee stance/fighter stance and begin to throw uppercuts, tensing your core as you do so.";
		exerciseTextArray[2] = "Holding a dumbbell in each hand, begin in the push up position and row the weight up to your ribcage, pause then repeat on the other side.";
		exerciseTextArray[3] = "Holding a dumbbell, step to the left, giving it to your imaginary teammate, then step back and jump taking an imaginary shot pushing the DB into the air. Repeat the other side.";
		exerciseTextArray[4] = "On your back, arms holding a dumbbell diagonally above your head, lift your legs vertically and together, then raise your legs and your hips off the floor. Pause and then repeat.";

		exerciseNameArray[0] = "Thrusters";
		exerciseNameArray[1] = "UpperCuts";
		exerciseNameArray[2] = "Renegade Rows";
		exerciseNameArray[3] = "DB Three Pointers";
		exerciseNameArray[4] = "DB Sky Pointers";

		firstPictureArray[0] = R.drawable.trexercise01_01;
		firstPictureArray[1] = R.drawable.trexercise02_01;
		firstPictureArray[2] = R.drawable.trexercise03_01;
		firstPictureArray[3] = R.drawable.trexercise04_01;
		firstPictureArray[4] = R.drawable.trexercise05_01;

		secondPictureArray[0] = R.drawable.trexercise01_02;
		secondPictureArray[1] = R.drawable.trexercise02_02;
		secondPictureArray[2] = R.drawable.trexercise03_02;
		secondPictureArray[3] = R.drawable.trexercise04_02;
		secondPictureArray[4] = R.drawable.trexercise05_02;

	}

	public void fireAbsExercise() {
		titleView.setBackgroundResource(R.drawable.fadisplay);
		exerciseTextArray = new String[4];
		exerciseNameArray = new String[4];
		firstPictureArray = new int[4];
		secondPictureArray = new int[4];

		exerciseTextArray[0] = "Holding a dumbbell in each hand, begin in the push up position and row the weight up to your ribcage, pause then repeat on other side.";
		exerciseTextArray[1] = "Begin in the push up position, then twist your body to the side, keeping it straight. Stack your feet on top of each other, as you raise your arm vertical. Hold. Then repeat the other side.  Keep the whole move under control.(optionally add a push up before each twist)";
		exerciseTextArray[2] = "Begin on hands and toes, then tense core and hop with feet together from side to side,landing softly and exploding to other side.";
		exerciseTextArray[3] = "In a push up position, begin to bring your knee to your chest, repeat other side. Build momentum.";

		exerciseNameArray[0] = "Renegade Rows";
		exerciseNameArray[1] = "T Bar Twist";
		exerciseNameArray[2] = "Breaker Hops";
		exerciseNameArray[3] = "Mountain Climbers";

		firstPictureArray[0] = R.drawable.faexercise01_01;
		firstPictureArray[1] = R.drawable.faexercise02_01;
		firstPictureArray[2] = R.drawable.faexercise03_01;
		firstPictureArray[3] = R.drawable.faexercise04_01;

		secondPictureArray[0] = R.drawable.faexercise01_02;
		secondPictureArray[1] = R.drawable.faexercise02_02;
		secondPictureArray[2] = R.drawable.faexercise03_02;
		secondPictureArray[3] = R.drawable.faexercise04_02;

	}

}

package com.gna.wwupro;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gna.wwupro.Infrastructure.BaseExerciseClass;

public class TimeOnWrapUpActivity extends BaseExerciseClass implements
		OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		if (getIntent().getExtras().getInt("value") == 1)
			setContentView(R.layout.ultimat_wrapup);
		else if (getIntent().getExtras().getInt("value") == 2)
			setContentView(R.layout.military_madness);
		else if (getIntent().getExtras().getInt("value") == 3)
			setContentView(R.layout.elevation_wrapup);
		else if (getIntent().getExtras().getInt("value") == 4)
			setContentView(R.layout.groovea);
		else if (getIntent().getExtras().getInt("value") == 5)
			setContentView(R.layout.grooveb);
		else if (getIntent().getExtras().getInt("value") == 6)
			setContentView(R.layout.resistance);
		else if (getIntent().getExtras().getInt("value") == 7)
			setContentView(R.layout.bonus);

		intializeView();
		intializeStates();
		intializeClickListener();
		keepScreenOn();

		LinearLayout rightArrowLayout = (LinearLayout) findViewById(R.id.rightArrowLayout);
		rightArrowLayout.setVisibility(View.GONE);

		TextView startLeftTextView = (TextView) findViewById(R.id.startLeftTextView);
		startLeftTextView.setText("Timer will Begin when you Press start");

		Button startBtn = (Button) findViewById(R.id.startBtn);
		startBtn.setOnClickListener(this);

	}

	private void intializeClickListener() {
		emailButton.setOnClickListener(this);
		//musicButton.setOnClickListener(this);
		leftArrowBtn.setOnClickListener(this);
		infoBtn.setOnClickListener(this);
		notesBarBtn.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
//		MediaPlayer mp = MediaPlayer.create(this, R.raw.button);
//		mp.start();
		if (v.getId() == R.id.emailBarBtn) {
			email();
		} 

		// else if (v.getId() == R.id.musicBarBtn) {
		// Intent musicPlayerIntent = new Intent(
		// "com.amazon.mp3.otter.action.SHOW_LIBRARY");
		// startActivity(musicPlayerIntent);
		// }
		else if (v.getId() == R.id.leftArrowBtn) {
			Intent intent = new Intent(this, NoofWrapsActvity.class);
			intent.putExtra("isTimedMode", true);
			intent.putExtra("value", getIntent().getExtras().getInt("value"));
			intent.putExtra("comingFrom", "front");
			startActivity(intent);
			this.finish();
		} else if (v.getId() == R.id.infoBtn) {
			Intent intent = new Intent(this, NoofWrapsActvity.class);
			intent.putExtra("value", getIntent().getExtras().getInt("value"));
			intent.putExtra("comingFrom", "back");
			startActivity(intent);
		} else if (v.getId() == R.id.startBtn) {
			Intent intent = new Intent(this, DisplayExerciseActivity.class);
			intent.putExtra("value", getIntent().getExtras().getInt("value"));
			intent.putExtra("isTimeMode", true);
			startActivity(intent);
			this.finish();
		} else if (v.getId() == R.id.notesBarBtn) {
			Intent intent = new Intent(this, NotesActivity.class);
			startActivity(intent);
		}
	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent(this, NoofWrapsActvity.class);
		intent.putExtra("isTimedMode", true);
		intent.putExtra("value", getIntent().getExtras().getInt("value"));
		intent.putExtra("comingFrom", "front");
		startActivity(intent);
		this.finish();
	}

}

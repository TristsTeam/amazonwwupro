package com.gna.wwupro;

import android.content.Intent;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.chartboost.sdk.Chartboost;
import com.chartboost.sdk.ChartboostDelegate;
import com.gna.wwupro.Infrastructure.CommonBaseClass;
import com.gna.wwupro.Infrastructure.WWUCommon;

public class HomeActivity extends CommonBaseClass implements OnClickListener {

	private Chartboost cb;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.home);
		intializeStates();
		intializeViews();
		keepScreenOn();

		this.cb = Chartboost.sharedChartboost();
		String appId = "511e62b017ba471834000083";
		String appSignature = "255158c870691dadecf9b03b08168950510da407";
		this.cb.onCreate(this, appId, appSignature, this.chartBoostDelegate);
		this.cb.startSession();
		cb.cacheMoreApps();
	}

	@Override
	protected void onStart() {
		super.onStart();

		this.cb.onStart(this);
	}

	@Override
	protected void onStop() {
		super.onStop();

		this.cb.onStop(this);
	}

	@Override
	public void onBackPressed() {
		if (this.cb.onBackPressed())
			// If a Chartboost view exists, close it and return
			return;
		else
			// If no Chartboost view exists, continue on as normal
			super.onBackPressed();
	}

	private void intializeViews() {

		StateListDrawable moreBtnStates = new StateListDrawable();
		moreBtnStates.addState(new int[] { android.R.attr.state_pressed },
				getResources().getDrawable(R.drawable.main_more_down));
		moreBtnStates.addState(new int[] {},
				getResources().getDrawable(R.drawable.main_more_norm));

		StateListDrawable infoBtnStates = new StateListDrawable();
		infoBtnStates.addState(new int[] { android.R.attr.state_pressed },
				getResources().getDrawable(R.drawable.info_down));
		infoBtnStates.addState(new int[] {},
				getResources().getDrawable(R.drawable.info_norm));

		StateListDrawable ultimateStates = new StateListDrawable();
		ultimateStates.addState(new int[] { android.R.attr.state_pressed },
				getResources()
						.getDrawable(R.drawable.main_ultimate_wrapup_down));
		ultimateStates.addState(new int[] {},
				getResources()
						.getDrawable(R.drawable.main_ultimate_wrapup_norm));

		StateListDrawable militaryStates = new StateListDrawable();
		militaryStates.addState(
				new int[] { android.R.attr.state_pressed },
				getResources().getDrawable(
						R.drawable.main_military_madness_down));
		militaryStates.addState(
				new int[] {},
				getResources().getDrawable(
						R.drawable.main_military_madness_norm));

		StateListDrawable elevationStates = new StateListDrawable();
		elevationStates.addState(new int[] { android.R.attr.state_pressed },
				getResources().getDrawable(R.drawable.mainelevation_down));
		elevationStates.addState(new int[] {},
				getResources().getDrawable(R.drawable.mainelevation_norm));

		StateListDrawable grooveAStates = new StateListDrawable();
		grooveAStates.addState(new int[] { android.R.attr.state_pressed },
				getResources().getDrawable(R.drawable.maingroove_a_down));
		grooveAStates.addState(new int[] {},
				getResources().getDrawable(R.drawable.maingroove_a_norm));

		StateListDrawable grooveBStates = new StateListDrawable();
		grooveBStates.addState(new int[] { android.R.attr.state_pressed },
				getResources().getDrawable(R.drawable.maingroove_b_down));
		grooveBStates.addState(new int[] {},
				getResources().getDrawable(R.drawable.maingroove_b_norm));

		StateListDrawable totalResistanceStates = new StateListDrawable();
		totalResistanceStates.addState(
				new int[] { android.R.attr.state_pressed }, getResources()
						.getDrawable(R.drawable.main_totalresistance_down));
		totalResistanceStates.addState(new int[] {}, getResources()
				.getDrawable(R.drawable.main_totalresistance_norm));

		StateListDrawable bonusStates = new StateListDrawable();
		bonusStates.addState(new int[] { android.R.attr.state_pressed },
				getResources().getDrawable(R.drawable.mainbonus_down));
		bonusStates.addState(new int[] {},
				getResources().getDrawable(R.drawable.mainbonus_norm));

		StateListDrawable ufaBarStates = new StateListDrawable();
		ufaBarStates.addState(new int[] { android.R.attr.state_pressed },
				getResources().getDrawable(R.drawable.ufabar_down));
		ufaBarStates.addState(new int[] {},
				getResources().getDrawable(R.drawable.ufabar_norm));

		ImageView moreBtn = (ImageView) findViewById(R.id.moreBtn);
		moreBtn.setBackgroundDrawable(moreBtnStates);
		moreBtn.setOnClickListener(this);

		ImageView infoBtn = (ImageView) findViewById(R.id.infoBtn);
		infoBtn.setBackgroundDrawable(infoBtnStates);
		infoBtn.setOnClickListener(this);

		ImageButton ufaBarBtn = (ImageButton) findViewById(R.id.ufaBarBtn);
		ufaBarBtn.setBackgroundDrawable(ufaBarStates);
		ufaBarBtn.setOnClickListener(this);

		StateListDrawable takeABreakStates = new StateListDrawable();
		takeABreakStates.addState(new int[] { android.R.attr.state_pressed },
				getResources().getDrawable(R.drawable.main_takeabreak_down));
		takeABreakStates.addState(new int[] {},
				getResources().getDrawable(R.drawable.main_takeabreak_norm));

		ImageView takeABreak = (ImageView) findViewById(R.id.takeABreak);
		takeABreak.setBackgroundDrawable(takeABreakStates);
		takeABreak.setOnClickListener(this);

		emailButton.setOnClickListener(this);
		// musicButton.setOnClickListener(this);
		notesBarBtn.setOnClickListener(this);

		ImageButton ultimateRightArrow = (ImageButton) findViewById(R.id.ultiamteRightArrow);
		ImageButton militaryMadnessArrow = (ImageButton) findViewById(R.id.mainMilitryRightArrow);
		ImageButton elevationArrow = (ImageButton) findViewById(R.id.elevationArrowRight);
		ImageButton grooveAArrow = (ImageButton) findViewById(R.id.mainGrooveARightArrow);
		ImageButton grooveBArrow = (ImageButton) findViewById(R.id.mainGrooveBRightArrow);
		ImageButton totalResistanceArrow = (ImageButton) findViewById(R.id.totalResistanceRightArrow);
		ImageButton bonusArrow = (ImageButton) findViewById(R.id.mainBonusRightArrow);

		ultimateRightArrow.setOnClickListener(this);
		militaryMadnessArrow.setOnClickListener(this);
		elevationArrow.setOnClickListener(this);
		grooveAArrow.setOnClickListener(this);
		grooveBArrow.setOnClickListener(this);
		totalResistanceArrow.setOnClickListener(this);
		bonusArrow.setOnClickListener(this);

		ImageView mainUltimateWrap = (ImageView) findViewById(R.id.ultimateWrapUp);
		ImageView mainMilitaryMadness = (ImageView) findViewById(R.id.mainMilitaryMadness);
		ImageView mainElevation = (ImageView) findViewById(R.id.mainElevation);
		ImageView mainGrooveA = (ImageView) findViewById(R.id.mainGrrooveA);
		ImageView mainGrooveB = (ImageView) findViewById(R.id.mainGrooveB);
		ImageView mainTotalResistance = (ImageView) findViewById(R.id.totalResistance);
		ImageView mainBonus = (ImageView) findViewById(R.id.mainBonus);

		mainUltimateWrap.setBackgroundDrawable(ultimateStates);
		mainMilitaryMadness.setBackgroundDrawable(militaryStates);
		mainElevation.setBackgroundDrawable(elevationStates);
		mainGrooveA.setBackgroundDrawable(grooveAStates);
		mainGrooveB.setBackgroundDrawable(grooveBStates);
		mainTotalResistance.setBackgroundDrawable(totalResistanceStates);
		mainBonus.setBackgroundDrawable(bonusStates);

		mainUltimateWrap.setOnClickListener(this);
		mainMilitaryMadness.setOnClickListener(this);
		mainElevation.setOnClickListener(this);
		mainGrooveA.setOnClickListener(this);
		mainGrooveB.setOnClickListener(this);
		mainTotalResistance.setOnClickListener(this);
		mainBonus.setOnClickListener(this);

	}

	@Override
	public void onClick(View view) {
		// MediaPlayer mp = MediaPlayer.create(this, R.raw.button);
		// mp.start();
		// TODO Auto-generated method stub
		if (view.getId() == R.id.moreBtn || view.getId() == R.id.ufaBarBtn) {
			Intent intent = new Intent(this, MoreActivity.class);
			startActivity(intent);
		} else if (view.getId() == R.id.infoBtn) {
			Intent intent = new Intent(this, InfoActivity.class);
			startActivity(intent);
		} else if (view.getId() == R.id.emailBarBtn) {
			email();
		}

		// else if (view.getId() == R.id.musicBarBtn) {
		// Intent musicPlayerIntent = new Intent(
		// "com.amazon.mp3.otter.action.SHOW_LIBRARY");
		// startActivity(musicPlayerIntent);
		// }
		else if (view.getId() == R.id.ultimateWrapUp
				|| view.getId() == R.id.ultiamteRightArrow) {
			Intent intent = new Intent(this, ModeSelectionActivity.class);
			intent.putExtra("value", 1);
			startActivity(intent);
		} else if (view.getId() == R.id.mainMilitaryMadness
				|| view.getId() == R.id.mainMilitryRightArrow) {
			Intent intent = new Intent(this, ModeSelectionActivity.class);
			intent.putExtra("value", 2);
			startActivity(intent);
		} else if (view.getId() == R.id.mainElevation
				|| view.getId() == R.id.elevationArrowRight) {
			Intent intent = new Intent(this, ModeSelectionActivity.class);
			intent.putExtra("value", 3);
			startActivity(intent);
		} else if (view.getId() == R.id.mainGrrooveA
				|| view.getId() == R.id.mainGrooveARightArrow) {
			Intent intent = new Intent(this, ModeSelectionActivity.class);
			intent.putExtra("value", 4);
			startActivity(intent);
		} else if (view.getId() == R.id.mainGrooveB
				|| view.getId() == R.id.mainGrooveBRightArrow) {
			Intent intent = new Intent(this, ModeSelectionActivity.class);
			intent.putExtra("value", 5);
			startActivity(intent);
		} else if (view.getId() == R.id.totalResistance
				|| view.getId() == R.id.totalResistanceRightArrow) {
			Intent intent = new Intent(this, ModeSelectionActivity.class);
			intent.putExtra("value", 6);
			startActivity(intent);
		} else if (view.getId() == R.id.mainBonus
				|| view.getId() == R.id.mainBonusRightArrow) {
			Intent intent = new Intent(this, ModeSelectionActivity.class);
			intent.putExtra("value", 7);
			startActivity(intent);
		} else if (view.getId() == R.id.notesBarBtn) {
			Intent intent = new Intent(this, NotesActivity.class);
			startActivity(intent);
		} else if (view.getId() == R.id.takeABreak) {
			if (WWUCommon.getInstance(this).isNetworkAvailable())
				onChartBoostClick();
			else {
				Intent intent = new Intent(this, GenericDialogActivity.class);
				intent.putExtra("message",
						"Internet connection is not available.");
				intent.putExtra("isQuit", false);
				startActivity(intent);
			}
		}
	}

	private ChartboostDelegate chartBoostDelegate = new ChartboostDelegate() {

		@Override
		public boolean shouldDisplayInterstitial(String location) {
			return true;
		}

		@Override
		public boolean shouldRequestInterstitial(String location) {
			return true;
		}

		@Override
		public void didCacheInterstitial(String location) {
		}

		@Override
		public void didFailToLoadInterstitial(String location) {

			Toast.makeText(HomeActivity.this,
					"Interstitial '" + location + "' Load Failed",
					Toast.LENGTH_SHORT).show();
		}

		@Override
		public void didDismissInterstitial(String location) {

			cb.cacheInterstitial(location);

		}

		@Override
		public void didCloseInterstitial(String location) {

		}

		@Override
		public void didClickInterstitial(String location) {

		}

		@Override
		public void didShowInterstitial(String location) {
		}

		@Override
		public boolean shouldDisplayLoadingViewForMoreApps() {
			return true;
		}

		@Override
		public boolean shouldRequestMoreApps() {

			return true;
		}

		@Override
		public boolean shouldDisplayMoreApps() {
			return true;
		}

		@Override
		public void didFailToLoadMoreApps() {
		}

		@Override
		public void didCacheMoreApps() {
		}

		@Override
		public void didDismissMoreApps() {
		}

		@Override
		public void didCloseMoreApps() {
		}

		@Override
		public void didClickMoreApps() {
		}

		@Override
		public void didShowMoreApps() {
		}

		@Override
		public boolean shouldRequestInterstitialsInFirstSession() {
			return true;
		}
	};

	public void onChartBoostClick() {
		this.cb.showMoreApps();
	}

}

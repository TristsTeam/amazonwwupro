package com.gna.wwupro;

import android.content.Intent;
import android.graphics.drawable.StateListDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.URLSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.gna.wwupro.Infrastructure.KeepScreenOnBaseClass;

public class CongratulationActivity extends KeepScreenOnBaseClass implements
		OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.congratulation);
		keepScreenOn();

		StateListDrawable emailBtnStates = new StateListDrawable();
		emailBtnStates.addState(
				new int[] { android.R.attr.state_pressed },
				this.getResources().getDrawable(
						R.drawable.large_shareemail_down));
		emailBtnStates.addState(
				new int[] {},
				this.getResources().getDrawable(
						R.drawable.large_shareemail_norm));

		StateListDrawable facebookStates = new StateListDrawable();
		facebookStates.addState(
				new int[] { android.R.attr.state_pressed },
				this.getResources().getDrawable(
						R.drawable.large_sharefacebook_down));
		facebookStates.addState(
				new int[] {},
				this.getResources().getDrawable(
						R.drawable.large_sharefacebook_norm));

		StateListDrawable twitterStates = new StateListDrawable();
		twitterStates
				.addState(
						new int[] { android.R.attr.state_pressed },
						this.getResources().getDrawable(
								R.drawable.large_sharetwitter_down));
		twitterStates.addState(
				new int[] {},
				this.getResources().getDrawable(
						R.drawable.large_sharetwitter_norm));

		StateListDrawable notesBarStates = new StateListDrawable();
		notesBarStates.addState(new int[] { android.R.attr.state_pressed },
				this.getResources().getDrawable(R.drawable.notesscreen_down));
		notesBarStates.addState(new int[] {},
				this.getResources().getDrawable(R.drawable.notesscreen_norm));

		ImageButton facebookBtn = (ImageButton) findViewById(R.id.facebookBtn);
		facebookBtn.setBackgroundDrawable(facebookStates);
		facebookBtn.setOnClickListener(this);

		ImageButton twitterBtn = (ImageButton) findViewById(R.id.twitterBtn);
		twitterBtn.setOnClickListener(this);
		twitterBtn.setBackgroundDrawable(twitterStates);

		ImageButton emailBtn = (ImageButton) findViewById(R.id.emailBtn);
		emailBtn.setOnClickListener(this);
		emailBtn.setBackgroundDrawable(emailBtnStates);

		ImageButton notesBtn = (ImageButton) findViewById(R.id.notesBtn);
		notesBtn.setBackgroundDrawable(notesBarStates);
		notesBtn.setOnClickListener(this);

		Button homeBtn = (Button) findViewById(R.id.homeBtn);
		homeBtn.setOnClickListener(this);

		TextView textView = (TextView) findViewById(R.id.textView);
		TextView textViewValue = (TextView) findViewById(R.id.textViewValue);
		if (getIntent().getExtras().getBoolean("isTimerMood")) {
			textViewValue.setText(getIntent().getExtras().getString(
					"CompleteValue"));
		} else {
			textViewValue.setVisibility(View.GONE);
			textView.setText(getString(R.string.CompleteText));
		}

	}

	@Override
	public void onClick(View view) {
		// MediaPlayer mp = MediaPlayer.create(this, R.raw.button);
		// mp.start();
		if (view.getId() == R.id.facebookBtn) {
			Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
			myWebLink.setData(Uri
					.parse("https://www.facebook.com/UltimateFitnessApp"));
			startActivity(myWebLink);
		} else if (view.getId() == R.id.twitterBtn) {
			Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
			myWebLink.setData(Uri.parse("https://twitter.com/UFAApp"));
			startActivity(myWebLink);
		} else if (view.getId() == R.id.emailBtn)
			email();
		else if (view.getId() == R.id.notesBtn) {
			Intent intent = new Intent(this, NotesActivity.class);
			startActivity(intent);
		} else if (view.getId() == R.id.homeBtn) {
			this.finish();
		}

	}

	private void email() {
		String url = "http://www.amazon.com/gp/mas/dl/android?p=com.gna.wwupro";

		SpannableStringBuilder builder = new SpannableStringBuilder();
		int start = builder.length();
		builder.append("Check out this great workout app I am using -");
		builder.append(url);
		int end = builder.length();

		builder.setSpan(new URLSpan(url), start, end,
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("message/rfc822");
		i.putExtra(Intent.EXTRA_EMAIL, new String[] { "" });
		i.putExtra(Intent.EXTRA_SUBJECT, "I workout! :)");
		i.putExtra(Intent.EXTRA_TEXT, builder);
		this.startActivity(Intent.createChooser(i, "Select application"));

	}

}
